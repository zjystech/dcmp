### 使用步骤

- 安装 `docker`
  - 安装: `curl -sSL https://g.wsl.fun/docker/install | VERSION="18.06.0-ce" CHANNEL="stable" sh`
  - 国内安装: `curl -sSL https://g.wsl.fun/docker/install | VERSION="18.06.0-ce" CHANNEL="stable" sh -s -- --mirror Aliyun --registry_mirror https://registry.docker-cn.com/`    
- 克隆 `dcmp` 到 `/work` 目录
  - `yum install git nodejs -y` 下载gitnpm
  - `git clone https://gitlab.com/zjystech/dcmp.git /work`
- 进入专门管理容器的容器工具
  - `cd /work/`
- 初始化
  - `npm run init` 创建数据容器需要的目录
  - `docker swarm init`
  - 设置 `docker-compose` 中的密码
  - `npm run update` 运行服务, 第一次下载镜像需要等蛮久的

### tips

- `npm run proxy:reload` 即可重载 `caddy` 配置
